# QCTIC Qiskit Provider

A Qiskit provider based on **Qiskit Aer** that may be used to interact with the quantum simulation platform **QCTIC** located at [CTIC](https://fundacionctic.org/).

Please check the `/notebooks` folder for some introductory examples of usage.
