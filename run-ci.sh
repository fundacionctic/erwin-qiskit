#!/usr/bin/env bash
set -e
pip install -U .[dev]
pytest --cov=qctic -v