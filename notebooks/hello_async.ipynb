{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# (Asynchronous) Introduction to the QCTIC provider for Qiskit\n",
    "\n",
    "Please note that this notebook uses the asyncio-based **asynchronous** interface of QCTIC. The regular synchronous interface specified by Qiskit (e.g. `job.result()`) is also available and introduced in the **hello_sync** notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import logging\n",
    "from datetime import datetime, timedelta\n",
    "from qiskit import QuantumCircuit, execute\n",
    "from qctic.provider import QCticProvider\n",
    "from qctic.utils import wait_result_async"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "logging.getLogger(\"qctic\").setLevel(logging.DEBUG)\n",
    "logging.basicConfig()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The QCTIC provider class expects two parameters: a Jupyter API token for authentication purposes and the URL of the QCTIC API server. Both can be retrieved automatically from this environment, however, you will have to provide them explicitly if the code is running outside of QCTIC's JupyterLab."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "qctic_provider = QCticProvider.from_env()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The QCTIC provider exposes a proxy backend for each one of Aer's simulators (i.e. unitary, statevector and qasm)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qctic_provider.backends()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "QCTIC conforms to the Qiskit interface so you can filter backends as usual."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qctic_provider.backends(memory=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can retrieve backends from the QCTIC _Provider_ using the regular Aer names."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qctic_backend = qctic_provider.get_backend(\"aer_simulator_statevector\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We are now going to simulate a simple quantum circuit in the QCTIC platform using the QASM simulator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "circuit = QuantumCircuit(2, 2)\n",
    "circuit.h(0)\n",
    "circuit.cx(0, 1)\n",
    "circuit.measure([0, 1], [0, 1])\n",
    "circuit.draw()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can run the circuit using the `execute` function as you would normally do with other providers. However, QCTIC presents one particularity when using the asynchronous interface: you need to pass an optional argument `async_submit=True` to `execute`. This call will return immediately and you should then `await` on `job.submit_task` for the HTTP request to finish."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "qctic_job = execute(circuit, qctic_backend, memory=True, shots=10, async_submit=True)\n",
    "await qctic_job.submit_task"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All the regular synchronous methods of the Qiskit Job interface (e.g. `status()`, `cancel()`) have an asynchronous counterpart (e.g. `status_async()`, `cancel_async()`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "await qctic_job.status_async()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `status` and `result` methods from the Job interface (and their asynchronous versions) use a cached version of the last API response when available. If you want to force a request to get the latest version of the job use the optional parameter `fetch`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "await qctic_job.status_async(fetch=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `remote_job` property returns the serialized _dict_ representation of the job that is cached internally."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qctic_job.remote_job.get(\"date_submit\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you now try to retrieve the result you will most probably observe that the call returns `None`, this is, the job is still pending and the result is not available."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "await qctic_job.result_async(fetch=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you need to close the current session and come back later (e.g. you expect the simulation to take a few hours to finish) you can retrieve an existing job from a backend instance using its ID."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "job_retrieved = await qctic_backend.retrieve_job_async(qctic_job.job_id())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may also search for jobs using the backend's `jobs` or `jobs_async` methods. Let's fetch the 5 most recent jobs from the past 10 minutes (please note that in this case the `status_async` calls won't cause the job instance to send a new request to the API)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ten_min_ago = datetime.now() - timedelta(minutes=10)\n",
    "jobs_found = await qctic_backend.jobs_async(date_start=ten_min_ago, limit=5)\n",
    "\n",
    "print(\"Found {} jobs\".format(len(jobs_found)))\n",
    "\n",
    "for job in jobs_found:\n",
    "    jstat = await job.status_async()\n",
    "    print(\"Job {} status: {}\".format(job.job_id(), jstat))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You may use the `wait_result_async` utility function to wait for the job to finish. This will start polling the server using a simple exponential backoff strategy (useful if you want to keep the session open for some reason)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "result = await wait_result_async(job_retrieved, timeout=300)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "result.get_counts(circuit)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result.get_memory(circuit)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
