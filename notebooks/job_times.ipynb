{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The QCTIC job lifecycle\n",
    "\n",
    "We may identify a series of **time costs** in the QCTIC job lifecycle:\n",
    "\n",
    "* Network latencies from the job submission request.\n",
    "* Wait times in the job queue.\n",
    "* Times for environment initialization and destruction once the job gets out of the queue into an active worker.\n",
    "* The actual simulation time.\n",
    "\n",
    "This notebook demonstrates the properties that enable the user to retrieve the durations of each step in the job lifecycle. This may be useful to evaluate whether the job is good fit for the simulation platform, this is, the time cost paid for all the other phases is acceptable when compared with the simulation time (i.e. the time the job spent being _executed_). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import logging\n",
    "import math\n",
    "from qiskit import QuantumCircuit, execute\n",
    "from qctic.provider import QCticProvider\n",
    "from qctic.utils import wait_result_async"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "logging.getLogger(\"qctic\").setLevel(logging.DEBUG)\n",
    "logging.basicConfig()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def input_state(circ, n):\n",
    "    \"\"\"n-qubit input state for QFT that produces output 1.\"\"\"\n",
    "\n",
    "    for j in range(n):\n",
    "        circ.h(j)\n",
    "        circ.p(-math.pi/float(2**(j)), j)\n",
    "\n",
    "def qft(circ, n):\n",
    "    \"\"\"n-qubit QFT on q in circ.\"\"\"\n",
    "\n",
    "    for j in range(n):\n",
    "        for k in range(j):\n",
    "            circ.cp(math.pi/float(2**(j-k)), j, k)\n",
    "        circ.h(j)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "QFTC_N = 18"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qftc = QuantumCircuit(QFTC_N, QFTC_N, name=\"QFT\")\n",
    "input_state(qftc, QFTC_N)\n",
    "qftc.barrier()\n",
    "qft(qftc, QFTC_N)\n",
    "qftc.barrier()\n",
    "\n",
    "for j in range(QFTC_N):\n",
    "    qftc.measure(j, j)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qctic_provider = QCticProvider.from_env()\n",
    "qctic_backend = qctic_provider.get_backend(\"aer_simulator_statevector\")\n",
    "qctic_job = execute(qftc, qctic_backend, shots=1024, async_submit=True)\n",
    "await qctic_job.submit_task\n",
    "result = await wait_result_async(qctic_job, max_sleep=1.0)\n",
    "result.get_counts(qftc)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qctic_job.time_submit?\n",
    "qctic_job.time_submit"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qctic_job.time_enqueued?\n",
    "qctic_job.time_enqueued"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qctic_job.time_running?\n",
    "qctic_job.time_running"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qctic_job.time_simulation?\n",
    "qctic_job.time_simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qctic_job.time_total?\n",
    "qctic_job.time_total"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As expected the sum of `time_submit`, `time_enqueued` and `time_running` is equal to `time_total`. Please note that `time_simulation` is contained within `time_running`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "total = qctic_job.time_submit + qctic_job.time_enqueued + qctic_job.time_running\n",
    "qctic_job.time_total - total"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The higher (closer to 1.0) the **simulation ratio** the better. A low simulation ratio means that most of the time was spent moving the job around and waiting instead of doing any work. In this case it would be reasonable to consider the possibility of running the simulation locally."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qctic_job.get_simulation_ratio?\n",
    "qctic_job.get_simulation_ratio()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In some cases the duration spent in the queue may be significant and unavoidable. You have the option of calculating the simulation ratio without taking the queue time into account."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qctic_job.get_simulation_ratio(queue=False)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
